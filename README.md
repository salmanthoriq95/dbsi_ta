# DATABASE SISTEM INFORMASI BUKU
Ini adalah aplikasi berbasis web untuk manajemen database tabel BUKU yang di development oleh :

- Krisma Lofitasari
- Lexintya Kanisha Dahuri
- Elisa Putri Larasatti
- Erintri Martina
- Najela Tsaqila Fatma Meimun
- Sinta Puspasari

## Pre-Requisite
Sebelum menjalankan aplikasi DATABASE SISTEM INFORMASI BUKU, ada beberapa aplikasi yang harus terinstal terlebih dahulu. Yaitu :

### MySql
MySql dibutuhkan untuk menyimpan data yang akan diolah. Pemilihan platform untuk menjalankan MySql silahkan pilih sesuai kenyamanan anda, tapi kami merekomendasikan menggunakan XAMPP karena appplikasi ini dibangun dengan menggunakan XAMPP.

Untuk instalasi XAMPP dan penggunaannya, silahkan kunjungi website resminya di [**XAMPP**](https://www.apachefriends.org/download.html)

### Node Js
Karena aplikasi ini menggunakan fullstack javascript, baik di front end maupun di di backend. Kami memilih menggunakan Node Js sebagai platform pendukung backend nya, sehingga kami menyarankan untuk menginstal Node Js terlebih dahulu sebelum menjalankan aplikasi ini

Untuk instalasi Node JS dan penggunaannya, silahkan kunjungi website resminya di [**NodeJs**](https://nodejs.org/en/)

untuk memastikan bahwa Node Js sudah terinstal dengan benar, silahkan buka terminal atau command prompt dan jalankan perintah berikut :
```bash
node --version
```

### NPM
Kami menggunakan beberapa module yang ada didalam NPM, sehingga sangat diharuskan untuk menginstal NPM terlebih dahulu sebelum penggunaan supaya aplikasi berjalan dengan baik.

Untuk instalasi NPM dan penggunaannya, silahkan kunjungi website resminya di [**NPM**](https://www.npmjs.com/)

untuk memastikan bahwa NM sudah terinstal dengan benar, silahkan buka terminal atau command prompt dan jalankan perintah berikut :
```bash
npm --version
```

Jika terminal menunjukan versi Node Js yang sudah terinstal, lanjutkan ke langkah berikutnya

## Cara Penggunaan
Sebelum menjalankan aplikasi pastikan semua MySql, NodeJs, dan NPM sudah terinstal di komputer Anda, Jika belum silahkan lihat bagian Pre-Requisite.

Jika sudah harap ikuti langkah-langkah berikut, supaya aplikasi berjalan dengan baik.

### Import Database ke MySql
Langkah pertama yang harus anda jalankan adalah dengan mengimpor database yang sudah disiapkan ke dalam MySql di komputer Anda. File backup MySql berada di direktori (folder) db_backup, dengan nama dbsi_ta.sql.

cara import database cukup mudah :
- Nyalakan XAMPP
- Buka browser, kemudian masuk ke localhost/phpmyadmin
- Buat database baru dengan nama yang sama, yaitu **dbsi_ta**
- Di bagian atas ada pilihan impor, silahkan klik tab impor
- Di bagian **file to import** ada tombol **browse** silahkan Anda tekan dan pilih file dbsi_ta yang ada di folder db_backup
- Tekan go di bagian pojok kanan bawah
- Selamat database sudah di buat

### Instal Modul-Modul NPPM
Setelah berhasil import database ke MySql andda, langkah selanjutnya adalah dengan menginstal modul-modul NPM yang di gunakan didalam aplikasi. Pastikan NPM sudah terinstal di komputer Anda, dan Anda terhubung dengan Internet.

Cara instal modul-modul NPM adalah :
- Buka terminal atau command prompt 
- masuk ke dalam direktori file dimana aplikasi ini di simpan
- ketikkan perintah 
```bash
npm install -f
```
- Tunggu sampai NPM selesai menginstal modul-modul uyang dibutuhkan
- Lihat struktur folder, jika sudah ada folder node_modules maka artinya seemua modul berhasil diinstal


### Jalankan Node JS
Masih di folder yang sama setelah instal modul-modul NPM, langkah selanjutnya adalah dengan menjalankan NodeJs. Pastikan NodeJS sudah terinstal di komputer Anda.

Cara menjalankan Node JS adalah :
- Buka terminal atau Command Prompt
- Pastikan direktori aktif berada di tempat dimana aplikasi ini disimpan
- Jalankan perintah
```bash
node index.js
```
- Maka terminal atau command prompt akan menamppilkan status 
```bash
Example app listening at http://localhost:3000
```
- Jika sudah begitu aplikasi DATABSE SISTEM INFORMASI BUKU sudah berjalan dengan baik

### Jalankan Aplikasi Melalui Browser
Setelah MySql berhasil di jalankan, modul-modul NPM berhasil diinstal, dan Node Js sudah full sevice, saatnya menjalankan aplikasi DATABASE SISTEM INFORMASI BUKU melalui browser.

Caranya sangat mudah, cukup buka browser kesayangan Anda kemudian masuk ke http://localhost:3000 maka akan muncul data-data dari tabel buku yang berasal dari database yang sudah Anda impor. Dan Anda sudah bisa melakukan manajemen tabel melalui browser.

HAPPY HACKING