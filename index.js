//express dependencies
const express = require('express')
const app = express()
const port = 3000

//use ejs as view engine
app.set('view engine', 'ejs')

//middleware static files
app.use(express.static('public'))

//importing validation
const Joi = require('joi')

//mysql config
const mysql = require('mysql');
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'dbsi_ta',
    multipleStatements: true
})

//parsing inut data
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

//import fs
const fs = require('fs')

//cors policy
const cors = require ("cors")
app.use(cors())

//setting accepted headers
app.use((req, res, next)=>{

    //website you wish allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");

    //Request method you wish to allow
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

    //Request Headers you wish to allow
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization")

    //Set to true if you need the website to include cookies in the requests sent
    //to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);

    //Pass to next layer of middleware
    next();
})

//read record end point
app.get('/', async (req, res) => {

    //get the body
    let search = req.query.search
    if(!search){search = ''}

    //validation
    //schema validation
    const schema = Joi.object({
        search : Joi.string().optional().allow('')
    })

    //validate input
    const {error} = schema.validate({
        search
    })

    //when validation error
    if(error){
        res.status(400)
        return res.send({
            success : false,
            message : 'Validation Error! Please Read Documentation',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //sql command
    const sqlCommand = `
        SELECT 
            *
        FROM
            buku
        WHERE
            idBuku LIKE '%${search}%'
            OR
            judulBuku LIKE '%${search}%'
            OR
            penerbitBuku LIKE '%${search}%'
            OR
            genreBuku LIKE '%${search}%'
            OR
            hargaBuku LIKE '%${search}%'
    `

    //promise connect to database
    const getList = new Promise((resolve) => {
        return conn.query(sqlCommand, (err, result) => {
            if (err) {
                console.log(err)
                return resolve('error');
            }
            return resolve(result);
        })
    })

    //get data
    getData = await getList
    const data = getData

    //return to client
    //when database is error
    if(getData === 'error') {
        res.status(400)
        res.render('index', {data : 'error'})
    }

    //OK
    res.status(200)
    res.render('index', {data: data})
})


//create new record end point
app.post('/buku', async (req, res) => {

    //get the body
    const judulBuku = req.body.judulBuku
    const penerbitBuku = req.body.penerbitBuku
    const genreBuku = req.body.genreBuku
    const hargaBuku = req.body.hargaBuku
    
    //validation
    //schema validation
    const schema = Joi.object({
        judulBuku       : Joi.string().required().max(100),
        penerbitBuku    : Joi.string().required().max(100),
        genreBuku       : Joi.string().required().max(50),
        hargaBuku       : Joi.number().required()
    })

    //validate input
    const {error} = schema.validate({
        judulBuku,
        penerbitBuku,
        genreBuku,
        hargaBuku
    })

    //when validation error
    if(error){
        res.status(400)
        return res.send({
            success : false,
            message : 'Validation Error! Please Read Documentation',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //sql command
    const sqlCommand = `
        INSERT INTO
            buku
            (
                judulBuku,
                penerbitBuku,
                genreBuku,
                hargaBuku
            )
        VALUES
            (
                '${judulBuku}',
                '${penerbitBuku}',
                '${genreBuku}',
                '${hargaBuku}'
            )
    `

    // promise connect to database
    const add = new Promise((resolve) => {
        conn.query(sqlCommand, (err, result) => {
            if (err) {
                console.log(err)
                return resolve('error');
            }
            return resolve(result);
        })
    })

    // get data
    const added = await add

    // return to client
    // when database is error
    if(added === 'error') {
        res.status(400)
        return res.send({
            success : false,
            message : 'Internal Server is Error! Please Contact Your Administration',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //OK
    res.status(200)
    return res.send({
        success : true,
        message : 'Success add new record!',
        method : req.method,
        url : req.url,
        data : {}
    })
})


//update record end point
app.put('/buku', async (req, res) => {
    
    //get the body
    const id = req.body.idBuku
    const judulBuku = req.body.judulBuku
    const penerbitBuku = req.body.penerbitBuku
    const genreBuku = req.body.genreBuku
    const hargaBuku = req.body.hargaBuku
    
    //validation
    //schema validation
    const schema = Joi.object({
        id              : Joi.number().required().min(1),
        judulBuku       : Joi.string().required().max(100),
        penerbitBuku    : Joi.string().required().max(100),
        genreBuku       : Joi.string().required().max(50),
        hargaBuku       : Joi.number().required()
    })

    //validate input
    const {error} = schema.validate({
        id,
        judulBuku,
        penerbitBuku,
        genreBuku,
        hargaBuku
    })

    //when validation error
    if(error){
        console.log(error)
        res.status(400)
        return res.send({
            success : false,
            message : 'Validation Error! Please Read Documentation',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //sql command
    const sqlCommand = `
        UPDATE
            buku
        SET
            judulBuku = '${judulBuku}',
            penerbitBuku = '${penerbitBuku}',
            genreBuku = '${genreBuku}',
            hargaBuku = '${hargaBuku}'
        WHERE
            idBuku = ${id}    
    `

    // promise connect to database
    const update = new Promise((resolve) => {
        conn.query(sqlCommand, (err, result) => {
            if (err) {
                console.log(err)
                return resolve('error');
            }
            return resolve(result);
        })
    })

    // get data
    const updated = await update

    // return to client
    // when database is error
    if(updated === 'error') {
        res.status(400)
        return res.send({
            success : false,
            message : 'Internal Server is Error! Please Contact Your Administration',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //OK
    res.status(200)
    return res.send({
        success : true,
        message : 'Success Updated Record!',
        method : req.method,
        url : req.url,
        data : {}
    })
})

//delete record end point
app.delete('/buku', async (req, res) => {
    //get the body
    const id = req.body.idBuku
    
    //validation
    //schema validation
    const schema = Joi.object({
        id: Joi.number().required().min(1),
    })

    //validate input
    const {error} = schema.validate({
        id,
    })

    //when validation error
    if(error){
        console.log(error)
        res.status(400)
        return res.send({
            success : false,
            message : 'Validation Error! Please Read Documentation',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //sql command
    const sqlCommand = `
        DELETE FROM
            buku
        WHERE
            idBuku = ${id}    
    `

    // promise connect to database
    const del = new Promise((resolve) => {
        conn.query(sqlCommand, (err, result) => {
            if (err) {
                console.log(err)
                return resolve('error');
            }
            return resolve(result);
        })
    })

    // get data
    const deleted = await del

    // return to client
    // when database is error
    if(deleted === 'error') {
        res.status(400)
        return res.send({
            success : false,
            message : 'Internal Server is Error! Please Contact Your Administration',
            method : req.method,
            url : req.url,
            data : {}
        })
    }

    //OK
    res.status(200)
    return res.send({
        success : true,
        message : 'Success Deleted record!',
        method : req.method,
        url : req.url,
        data : {}
    })
})


//default end point
app.use((req, res) => {
    res.send({
        success : false,
        message : 'wrong end point',
        method : req.method,
        url : req.url,
        data : {}
    })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})