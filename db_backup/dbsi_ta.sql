-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 15, 2021 at 01:51 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsi_ta`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `idBuku` int(20) NOT NULL,
  `judulBuku` varchar(100) NOT NULL,
  `penerbitBuku` varchar(100) NOT NULL,
  `genreBuku` varchar(50) NOT NULL,
  `hargaBuku` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`idBuku`, `judulBuku`, `penerbitBuku`, `genreBuku`, `hargaBuku`) VALUES
(1, 'Bulan', 'Gramedia Pustaka Utama ', 'Fiksi', 150000),
(2, 'Matahari', 'Gramedia Pustaka Utama ', 'fiksi', 145000),
(3, 'Bintang', 'Gramedia Pustaka Utama ', 'fiksi', 180000),
(4, 'Bung Karno : Penyambung Lidah Rakyat', 'Yayasan Bung Karno', 'Boigrafi', 160000),
(5, 'Sherlock Holmes : Skandal Bohemia', 'Gramedia Pustaka Utama', 'fiksi', 130000),
(6, 'Naruto : Shippuden Vol 32', 'Elex Media Komputindo', 'Komik', 90000),
(11, 'apa aja', 'siapa aja', 'terserah', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`idBuku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `idBuku` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
